<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
            DB::table('cities')->insert([
                'name' => 'Brasília',
            ]);
            DB::table('cities')->insert([
                'name' => 'Taguatinga',
            ]);
            DB::table('cities')->insert([
                'name' => 'Ceilândia',
            ]);
            DB::table('cities')->insert([
                'name' => 'Sudoeste',
            ]);
            DB::table('cities')->insert([
                'name' => 'Cruzeiro',
            ]);
            DB::table('cities')->insert([
                'name' => 'Octogonal',
            ]);
            DB::table('cities')->insert([
                'name' => 'Asa Norte',
            ]);
            DB::table('cities')->insert([
                'name' => 'Samambaia',
            ]);
            DB::table('cities')->insert([
                'name' => 'Riacho Fundo',
            ]);
            DB::table('cities')->insert([
                'name' => 'Guará',
            ]);
       
    }
}
