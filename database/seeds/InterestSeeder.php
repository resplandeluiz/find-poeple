<?php

use Illuminate\Database\Seeder;

class InterestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            
            DB::table('interests')->insert([
                'name' => 'Sertanejo',
            ]);
            DB::table('interests')->insert([
                'name' => 'Rock',
            ]);
            DB::table('interests')->insert([
                'name' => 'Pop',
            ]);
            DB::table('interests')->insert([
                'name' => 'Rap',
            ]);
            DB::table('interests')->insert([
                'name' => 'MPB',
            ]);
            DB::table('interests')->insert([
                'name' => 'Lambada',
            ]);
            DB::table('interests')->insert([
                'name' => 'Soul',
            ]);
            DB::table('interests')->insert([
                'name' => 'Gospel',
            ]);
            DB::table('interests')->insert([
                'name' => 'Clarinete',
            ]);
        
    }
}
