<?php

namespace App\Http\Controllers;

use App\Interest;
use Validator;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\City;
use App\UserInterest;

class UserController extends Controller
{


    protected $validatorRules = [
        'name' => 'required|max:255',
        'date_birth' => 'required|date',
        'city_id' => 'required',
        'email' => 'unique:users,email|email',
        'picture' => 'required|file|mimes:jpeg,bmp,png'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(4);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        $interests = Interest::all();
        return view('users.form', compact('cities', 'interests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), $this->validatorRules);

        if ($validator->fails()) {
            Session::flash('error', $validator->errors());

            return Redirect::back()
                ->withInput();
        }

        $user = new User();
        $user->fill($request->all());
        $user->picture = FileController::upload($request->picture, 'IMG');
        $user->save();

        if ($request->interest) {
            foreach ($request->interest as $key => $value) {
                UserInterest::create([
                    'user_id' => $user->id,
                    'interest_id' => $value
                ]);
            }
           
        }

        Session::flash('message', 'Cadastrado com sucesso!');
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);    
       
        if(!$user){
            Session::flash('error', 'Usuário não existe!');
            return redirect('/users');
        }
        $userCities = User::where('city_id', $user->city_id)->where('id','<>',$user->id)->get();       
        $idInterest = (new InterestController)->getMyInterests($id);
        $usersIterests = (new UserInterest)->getSimilarUsers($id, $idInterest);

        return view('users.show', compact('user','userCities','usersIterests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserInterest::where('user_id',$id)->delete();
        User::find($id)->delete();

        Session::flash('message', 'Usuário deletado com sucesso!');
        return Redirect::back();
    }
}
