<?php

namespace App\Http\Controllers;

use App\UserInterest;


class InterestController extends Controller
{   

    public function getMyInterests($userId){

        return UserInterest::where('user_id',$userId)->get('interest_id');

    }


    
}
