<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = ['name', 'picture', 'email', 'date_birth', 'city_id'];

    public function city(){
        return $this->hasOne('App\City','id','city_id');
    }

    public function interests(){
        
        return $this->belongsToMany('App\Interest','user_interests');
    }

   

    
}
