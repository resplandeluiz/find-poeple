<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInterest extends Model
{
    protected $tablename = 'user_interests';
    protected $fillable = ['user_id', 'interest_id'];


    public function getSimilarUsers($id, $interest_id)
    {
        $idUsers = UserInterest::whereIn('interest_id', $interest_id)->where('user_id','<>', $id)->distinct('user_id')->get('user_id');       
        return User::whereIn('id',$idUsers)->get();
    }
}
