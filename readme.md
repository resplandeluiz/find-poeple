## Passos

    você precisa ter o composer eo laravel instalado


    Baixe o projeto:
    git pull 

    Acesse a pasta:
    cd find-people/

    Instale os pacotes:
    composer install



    Crie uma cópia do .env.example para .env

    Altere o bloco de notação de referenci ao BD

    DB_CONNECTION=sqlite
    DB_DATABASE=./database/findPeopleDB.sqlite
    
    Execute
    php artisan key:generate

    Adicione as tabelas e os dados prontos no banco:
    php artisan migrate:fresh --seed

    Crie um link para acessar as fotos:
    php artisan storage:link

    Suba o servidor:
    php artisan serve


--------------------------

Desenvolvido por : Luiz Resplande