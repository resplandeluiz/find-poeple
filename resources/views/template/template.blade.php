@include('template.header')
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
  <a class="navbar-brand mr-auto mr-lg-0" href="/">Find People</a>
  <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/users/create">Cadastrar</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/users">Usuários</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="https://gitlab.com/resplandeluiz/find-poeple" target="_blank">GitLab</a>
      </li>

    </ul>

  </div>
</nav>


@yield('content')

@if(Session::has('message'))
<p class="alert alert-success mt-3">{{ Session::get('message') }}</p>
@endif

@if(Session::has('error'))
<p class="alert alert-danger mt-3">{{ Session::get('error') }}</p>
@endif
@include('template.footer')