@extends('template.template')

@section('content')
<div class="row" style="margin-top:85px!important">
    <div class="col-12">
        <h4 class="mb-3">Cadastrar usuário</h4>
    </div>
    <div class="col-6">

        <form class="needs-validation" method="POST" action="/users" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col">
                    <label for="name">Nome *</label>
                    <input type="text" class="form-control" id="name" placeholder="" value="" required name="name">

                </div>

            </div>

            <div class="mb-3">
                <label for="picture">Foto *</label>
                <div class="input-group">

                    <input type="file" class="form-control" id="picture" placeholder="Picture" name="picture" required>

                </div>
            </div>

            <div class="mb-3">
                <label for="email">Email </label>
                <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
            </div>

            <div class="date_birth-3">
                <label for="address">Data de Nascimento *</label>
                <input type="date" class="form-control" id="date_birth" name="date_birth" placeholder="" required>

            </div>
    </div>
    <div class="col-6">
        <div class="row">
            <div class="col-12">
                <label for="city_id">Cidade</label>
                <select class="custom-select d-block w-100" id="city_id" name="city_id" required>
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>



            </div>
            <div class="col-12">
                <label for="interest_id">Interesses</label>
                <div class="col-12 row  ">
                    <ul class="col-6">
                        @foreach($interests as $interest)
                        <li class="list-group-item"> <input class="form-check-input" type="checkbox" value="{{ $interest->id }}" name="interest[]" id="defaultCheck1">{{ $interest->name }}</li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>

        <hr class="mb-4">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Salvar</button>
        </form>
    </div>
</div>

@endsection

@section('js')

$('#interest_id').selectpicker();

@endsection