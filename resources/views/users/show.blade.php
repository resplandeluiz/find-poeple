@extends('template.template')

@section('content')
<form method="post" style="margin-top:85px">
    <div class="row">
        <div class="col-md-4">
            <div class="profile-img">
                <img src="{{ asset('storage/'. $user->picture) }}" width="285px" height="183px" alt="" />

            </div>
        </div>
        <div class="col-md-6">
            <div class="profile-head">
                <h5>
                    {{$user->name}}
                </h5>
                <h6>
                    Mora em : {{$user->city->name}}
                </h6>
                <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Usuários próximos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="skill-tab" data-toggle="tab" href="#skill" role="tab" aria-controls="skill-tab" aria-selected="false">Usuários que tem o mesmo gosto</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-2">
            <a href="/users" class="btn btn-info btn-sm" name="btnAddMore" value="Voltar">Voltar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="profile-work">
                <p>Interesses</p>
                @foreach($user->interests as $interest)
                <a href="">{{ $interest->name }}</a><br>
                @endforeach
            </div>
        </div>
        <div class="col-md-8">
            <div class="tab-content profile-tab" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">
                        <div class="col-md-6">
                            <label>Name</label>
                        </div>
                        <div class="col-md-6">
                            <p>{{$user->name}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Email</label>
                        </div>
                        <div class="col-md-6">
                            <p>{{$user->email}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Data de nascimento</label>
                        </div>
                        <div class="col-md-6">
                            <p>{{$user->date_birth}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Cidade</label>
                        </div>
                        <div class="col-md-6">
                            <p>{{$user->city->name}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    @foreach($userCities as $user)

                    <div class="media text-muted pt-3">
                        <img data-src="{{ asset('storage/'. $user->picture) }}" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="{{ asset('storage/'. $user->picture) }}" data-holder-rendered="true">

                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">

                            <strong class="d-block text-gray-dark">{{ $user->name }}</strong>
                            <b>Cidade : </b> {{$user->city->name }}

                        </p>
                        <div class="col-2">


                            <a class="btn btn-info btn-sm" href="/users/{{$user->id}}" style="margin-right: 5px"><i class="fas fa-eye"></i></a>


                        </div>
                    </div>


                    @endforeach
                </div>
                <div class="tab-pane fade" id="skill" role="tabpanel" aria-labelledby="skill-tab">

                    @foreach($usersIterests as $user)

                    <div class="media text-muted pt-3">
                        <img data-src="{{ asset('storage/'. $user->picture) }}" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="{{ asset('storage/'. $user->picture) }}" data-holder-rendered="true">

                        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">

                            <strong class="d-block text-gray-dark">{{ $user->name }}</strong>
                            <b>Cidade : </b> {{$user->city->name }}

                        </p>
                        <div class="col-2">


                            <a class="btn btn-info btn-sm" href="/users/{{$user->id}}" style="margin-right: 5px"><i class="fas fa-eye"></i></a>


                        </div>
                    </div>


                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>
@endsection