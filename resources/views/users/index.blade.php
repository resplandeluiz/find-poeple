@extends('template.template')

@section('content')
<div class="my-3 p-3 bg-white rounded box-shadow" style="margin-top:85px!important">
    <h6 class="border-bottom border-gray pb-2 mb-0">Usuários</h6>
    @foreach($users as $user)

    <div class="media text-muted pt-3">
        <img data-src="{{ asset('storage/'. $user->picture) }}" alt="32x32" class="mr-2 rounded" style="width: 32px; height: 32px;" src="{{ asset('storage/'. $user->picture) }}" data-holder-rendered="true">

        <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">

            <strong class="d-block text-gray-dark">{{ $user->name }}</strong>
            <b>Cidade : </b> {{$user->city->name }}

        </p>
        <div class="col-2">


            <a class="btn btn-info btn-sm" href="/users/{{$user->id}}" style="margin-right: 5px"><i class="fas fa-eye"></i></a>

            <form action="/users/{{$user->id}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-danger btn-sm" style="margin-right: 5px"><i class="fas fa-trash"></i></button>
            </form>
        </div>
    </div>


    @endforeach
    <div class="col-12 text-center mt-3">
        {{ $users->links() }}
    </div>


</div>
@endsection